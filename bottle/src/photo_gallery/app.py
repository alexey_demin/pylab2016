# coding: utf-8
from pathlib import Path

from bottle import jinja2_view
from bottle import route
from bottle import run
from bottle import static_file

from .config import COLUMN_COUNT
from .config import DATABASE_FILE
from .config import PHOTOS_DIR
from .config import PHOTOS_URL
from .utils import get_database_connection
from .utils import get_photos_count
from .utils import get_photos_info
from .utils import get_rows


@route('/photos')
@jinja2_view('index.html')
def photos():
    """Страница с фотографиями галлереи."""
    with get_database_connection(DATABASE_FILE) as dbc:
        photos_count = get_photos_count(dbc)
        photos_info = get_photos_info(dbc)

        rows = get_rows(photos_info, photos_count, COLUMN_COUNT)

        return dict(
            PHOTOS_URL=PHOTOS_URL,
            rows=rows,
        )


@route('/photos/<filename>')
def get_file(filename):
    """Файл фотографии."""
    return static_file(filename, root=PHOTOS_DIR)


if __name__ == '__main__':
    from bottle import TEMPLATE_PATH

    TEMPLATE_PATH[:] = [str(Path(__file__).parent / 'templates')]

    run(host='localhost', port=8080)
